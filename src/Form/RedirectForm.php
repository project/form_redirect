<?php

namespace Drupal\form_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure redirect_form settings for this site.
 */
class RedirectForm extends ConfigFormBase {
  /**
 * @var string Config settings */
  const SETTINGS = 'redirect_form.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'redirect_form_setting';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $name_field = $form_state->get('num_of_path');
    $form['#tree'] = TRUE;

    // Get all form_id from form_id.settings config file.
    $redirect_form_config = \Drupal::configFactory()->get('redirect_form.settings')->get('redirect_form_config');
    $form['redirect_form_fieldset'] = [
      '#type' => 'fieldset',
      '#prefix' => "<div id='form-redirect-fieldset-wrapper'>",
      '#suffix' => '</div>',
    ];
    if(empty($redirect_form_config)){
      $count_redirect_form_config=1;
    } else {
      $count_redirect_form_config = count($redirect_form_config);
    }
    if (empty($name_field)) {
      $name_field = $form_state->set('num_of_path', $count_redirect_form_config);
    }
    for ($i = 1; $i <= $form_state->get('num_of_path'); $i++) {
      if($i%2==0){
        $class_name='form-redirect-field-even';
      } else{
        $class_name='form-redirect-field-odd';
      }
      $form['redirect_form_fieldset']['redirect_form_config'][$i]['custom_form_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Form ID'),
        '#default_value' => $redirect_form_config[$i]['custom_form_id'],
        '#prefix' => '<div class="form-redirect-field '.$class_name.'">',
        '#suffix' => '</div>',
      ];
      $form['redirect_form_fieldset']['redirect_form_config'][$i]['redirect_path'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Redirect Path'),
        '#default_value' => $redirect_form_config[$i]['redirect_path'],
        '#prefix' => '<div class="form-redirect-field '.$class_name.'">',
        '#suffix' => '</div>',
      ];
    }
    $form['redirect_form_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['redirect_form_fieldset']['actions']['add_form_redirect_path'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => "form-redirect-fieldset-wrapper",
      ],
      '#prefix' => '<div class="redirect-form-button">',
      '#suffix' => '</div>',
    ];
    if ($form_state->get('num_of_path') > 1) {
      $form['redirect_form_fieldset']['actions']['remove_form_redirect_path'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => "form-redirect-fieldset-wrapper",
        ],
        '#prefix' => '<div class="redirect-form-button">',
        '#suffix' => '</div>',
      ];
    }
    $form['#attached']['library'][] = 'form_redirect/form_redirect_css';
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_path');
    return $form['redirect_form_fieldset'];
  }

  /**
   *
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_path');
    $add_button = $name_field + 1;
    $form_state->set('num_of_path', $add_button);
    $form_state->setRebuild();
  }

  /**
   *
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_of_path');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_of_path', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $redirect_form_config = $form_state->getValue(['redirect_form_fieldset', 'redirect_form_config']);
    if(!empty($redirect_form_config)){
      $count=0;
      $form_id_array=[];
      foreach ($redirect_form_config as $key => $value) {
        if(!in_array($value['custom_form_id'], $form_id_array)){
          $form_id_array[]=$value['custom_form_id'];
        } else{
          $count++;
        }
      }
      if($count>0){
        $form_state->setErrorByName('custom_form_id', $this->t('Duplicate form id not allowed.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $redirect_form_config = $form_state->getValue(['redirect_form_fieldset', 'redirect_form_config']);
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('redirect_form_config', $redirect_form_config)
      ->save();

    $router_builder = \Drupal::service('router.builder');
    $router_builder->rebuild();

    parent::submitForm($form, $form_state);
  }
}
