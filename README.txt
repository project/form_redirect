-- SUMMARY --

  This is a Form Redirect Module.
    * It permits admin to define redirect path for different forms by specifying its unique id.

  For a full description of the module, visit the project page:
    https://www.drupal.org/project/form_redirect

  To submit bug reports and feature suggestions, or to track changes:
    http://drupal.org/project/issues/form_redirect


-- INSTALLATION --


  1. Download the module and extract the files.
  2. Upload the entire form_redirect folder into your Drupal modules/contrib
     directory.
  3. Enable the Form Redirect module by navigating to:
     Administration > Modules
  4. Configure settings by navigating to:
     Administration > Configuration > Form ID Redirect


-- USAGE --

  * This module provides admin a feature to add form redirect path for all available
    forms.

  * After installing the module, navigate to :-
    Administration > Configuration > Form ID Redirect

  * You will be able to see form where you can add form id and redirect path respectively.




-- CREDITS --

  MAINTAINER:

  * Gaurav Kapoor

  * Sandeep Jangra
